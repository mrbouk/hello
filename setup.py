#!/usr/bin/env python

import setuptools

setuptools.setup(
    name="hello-reda",
    version="0.0.1",
    py_modules=['hello'],
    scripts=['hello'],

    python_requires='>=3.6',

    description="Un petit package pour dire bonjour",
    author="Reda BOUKENTAR",
    author_email="reda.boukentar.esv@gmail.com",
    url="https://gitlab.com/mrbouk/hello",

    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],

)
