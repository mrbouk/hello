#!/usr/bin/env python

import hello

def test_hello():
	HELLO = hello.hello()
	NAME = hello.cli()
	assert HELLO == f"Hello {NAME}"
