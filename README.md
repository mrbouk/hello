Choix d’une bibliothèque python :

En vérifiant la documentation officielle de python, on voit qu’il existe des modules par défauts capable de retrouver exactement les données que l’on veut exploiter dans notre code.

sys.argv() pour avoir la liste des arguments passés en ligne de commande.

getopt, optparse et argparse, des modules bien plus complets pour gérer les arguments (option, paramètres, manuel)

https://stackoverflow.com/questions/35365344/python-sys-argv-and-argparse

J’ai choisi de me passer d’une bibliothèque, celles-ci nécessitants d’importer d’autres packages supplémentaires, je pense quecela ferait qu’on mon programme ne fonctionnera pas sur une machine qui ne les as pas.

J’ai aussi choisi sys.arg a argparse, étant donné la simplicité de l’option à implémenter (une option le prénom).

L’avantage de ce choix sera que j’aurais un code simple à lire et à comprendre, don à modifier je pense.

L’inconvénient majeurs, par contre, sera que je serais limité par la simplicité de mes choix, si je voulais ajouter plus d’option par exemple, et un manuel, j’aurais du mal je pense. 

**********************************************************************************************
Shebang :
Je suis toujours débutant, j’ai tendance à vouer une confiance totale au développeur de python et à leur documentation. Car elles m’ont toujours mené vers le bon choix dans mes recherches pour le moment.

Concernant les autres ressources, ma confiance est moyenne, celles-ci pouvant êtres obsolètes, ou erronées, cela me demande généralement un travail de vérification. Pour vérifier la validité je me base sur plusieurs arguments : l’approbation globale, la répétitivité des réponses (à quelle fréquence je retrouve cette pratique sur d’autres sources), l’avis des collègues plus expérimentés de la License, et pour finir via l’essai de la dite solution.

Comme choix de shebang j’ai choisi #!/usr/bin/env python.

Son utilisation est un sujet assez fréquent sur les forums populaires, chaque année un débutant comme moi çe retrouve a questionner la meilleure utilisation de shebang sur python et dans chaque topic j’ai trouvé la même réponse : « #!/usr/bin/python lance python si il est dans l’environnement /usr/bin/python, #!/usr/bin/env python lanceras le premier environnement qui est précisé dans $PATH » faire ce choix rendra mon programme plus flexible (pour les systèmes ou il n’y a pas de « usr/bin/python » par exemple ).

Liens :

https://stackoverflow.com/questions/17846908/proper-shebang-for-python-script

https://stackoverflow.com/questions/2429511/why-do-people-write-usr-bin-env-python-on-the-first-line-of-a-python-script

https://stackoverflow.com/questions/5709616/whats-the-difference-between-python-shebangs-with-usr-bin-env-rather-than-hard

**********************************************************************************************

Les manières pythioniques :

Pour Ces questions j’avais pris de l’avance sans le savoir étant donné que l’on avait abordé le sujet dans nos premiers cours avec M.Guijarro.
En m’appuyant sur internet et mes notes de cours, j’ai compris que « la manière pythionique de coder » consiste à le faire en suivant bien les recommandations de la PEP8 un guide officiel.

2.1) 

"Il est plus facile de demander pardon que la permission"

Python fait partie des langages qui considèrent qu’il est plus simple d’essayer puis de gérer les erreurs, que de demander la permission en amont.

Pour gérer l’ouverture d’un fichier, par exemple, on préférera faire appel à open, et traiter les différentes exceptions qui pourraient se produire (fichier inexistant, droits insuffisants, etc.), plutôt que de tester une à une ces différentes conditions.

"Les erreurs ne devraient jamais se produire silencieusement, À moins d’être explicitement tues"

Quand une erreur se produit c’est qu’il y a un problème, quel qu’il soit. Ce problème ne doit jamais être masqué au développeur sauf si celui-ci à choisi d’ignorer une erreur en particulier, car celle-ci est attendue dans ce cas précis. Mais il le fera de façon explicite, avec un bloc try/except

2.2)

"L’explicite est préférable à l’implicite."

Un développeur doit pouvoir lire un code Python sans se demander sans cesse ce que fait telle ou telle ligne. Utiliser des noms et des constructions explicites permet de limiter ce genre de problèmes.

Importer seulement les méthodes voulue en les renommant permettra à notre code d’être mieux compris et mieux lu.


**********************************************************************************************
PYLINT :

Module hello


hello.py:33:0: C0304: Final newline missing (missing-final-newline)

hello.py:1:0: C0114: Missing module docstring (missing-module-docstring)

hello.py:9:0: C0116: Missing function or method docstring (missing-function-docstring)

hello.py:20:4: W0702: No exception type(s) specified (bare-except)

hello.py:26:0: C0116: Missing function or method docstring (missing-function-docstring)

hello.py:32:4: C0103: Constant name "prenom" doesn't conform to UPPER_CASE naming style (invalid-name)

Your code has been rated at 6.84/10 (previous run: 6.84/10, +0.00)

*********************************************************************

CORRECTIFS:

"Constant name "prenom" doesn't conform to UPPER_CASE naming style (invalid-name)" : 
(solution) j'ai mis la variable concernée en majuscule.

"C0304: Final newline missing (missing-final-newline)":
(solution) j'ai laissé une ligne vide à la fin de mon code.

"W0702: No exception type(s) specified (bare-except)" :
(solution) c'est une erreur qui souligne mon manque d'exceptions, etant donné la simplicitée du code et le non besoin d'except j'ai désactivée cette alerte avec la docstring "# pylint: disable=W0702"

*********************************************************************

PYLINT APRES LE CORRECTIF:

Module hello

hello.py:1:0: C0114: Missing module docstring (missing-module-docstring)

hello.py:9:0: C0116: Missing function or method docstring (missing-function-docstring)

hello.py:26:0: C0116: Missing function or method docstring (missing-function-docstring)

Your code has been rated at 8.42/10 (previous run: 2.63/10, +5.79)

************************************************************************

Documentation :

J’ai opté pour des « One-line Docstrings », je trouve cette façon de taper des commentaires non-agressive à lire, cela à mon avis, permet mieux de séparer les commentaires du code réel comparé a #, que j’ai quand même du utiliser pour éviter un bug.

*************************************************************************

PIP :

Paquet créé, on peut l’installer via la commande :

"pip install hello-bouk"

UTILISATION :

taper "hello" suivi ou non d'une personne à saluer.

si le prenom dépasse 16 charactère, vous serez surnommé "Long named folk"

*************************************************************************

pytest :

pour mon test j’ai choisi de verifier que la saisie corespond bien à la saisie voulu, le code passe bien le test.

*************************************************************************






